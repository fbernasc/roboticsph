function EEGTriggers(handle, address, triggerType, connection)
%EEGTriggers Sends triggers to an EEG system
%   Currentyl this function is capable to send triggers to a biosemi EEG
%   system through either serial or parallel port connections
    
    triggerDuration = 0.005;

    Triggers.init        = 250;
    Triggers.stop        = 251;
    Triggers.beginExp    = 10;
    Triggers.endExp      = 11;
    Triggers.beginTrial  = 20;
    Triggers.endTrial    = 21;
    Triggers.frontTouch  = 30;
    Triggers.backTouch   = 31;
    Triggers.buttonPress = 40;
    
    
    if strcmp(connection, 'biosemi:serial')
        if strcmp(triggerType, 'start') % initate recording
            IOPort('Write', handle, uint8(Triggers.init));
        elseif strcmp(triggerType, 'stop') % stop recording
            IOPort('Write', handle, uint8(Triggers.stop));
        elseif strcmp(triggerType, 'begin') % start of experiment
            IOPort('Write', handle, uint8(Triggers.beginExp));
        elseif strcmp(triggerType, 'end') % end of experiment
            IOPort('Write', handle, uint8(Triggers.endExp));
        elseif strcmp(triggerType, 'begin_trial') % begin trial
            IOPort('Write', handle, uint8(Triggers.beginTrial));
        elseif strcmp(triggerType, 'finish_trial') % finish trial
            IOPort('Write', handle, uint8(Triggers.endTrial));
        elseif strcmp(triggerType, 'front_touch') % participant touched the front
            IOPort('Write', handle, uint8(Triggers.frontTouch));
        elseif strcmp(triggerType, 'back_touch') % participant was touched on the back
            IOPort('Write', handle, uint8(Triggers.backTouch));
        elseif strcmp(triggerType, 'button_press') % participant produced a touch response
            IOPort('Write', handle, uint8(Triggers.buttonPress));
        end
        pause(triggerDuration);
        IOPort('Write', handle, uint8(0));
        
    elseif strcmp(connection, 'biosemi:parallelport')
        if strcmp(triggerType, 'start') % initate recording
            io64(handle, address, Triggers.init);
        elseif strcmp(triggerType, 'stop') % stop recording
            io64(handle, address, Triggers.stop);
        elseif strcmp(triggerType, 'begin') % start of experiment
            io64(handle, address, Triggers.beginExp);
        elseif strcmp(triggerType, 'end') % end of experiment
            io64(handle, address, Triggers.endExp);
        elseif strcmp(triggerType, 'begin_trial') % begin trial
            io64(handle, address, Triggers.beginTrial);
        elseif strcmp(triggerType, 'finish_trial') % finish trial
            io64(handle, address, Triggers.endTrial);
        elseif strcmp(triggerType, 'front_touch') % participant touched the front
            io64(handle, address, Triggers.frontTouch);
        elseif strcmp(triggerType, 'back_touch') % participant was touched on the back
            io64(handle, address, Triggers.backTouch);
        elseif strcmp(triggerType, 'button_press') % participant produced a touch response
            io64(handle, address, Triggers.buttonPress);
        end
        pause(triggerDuration);
        io64(handle, address, 0);
        
    end
    
end

