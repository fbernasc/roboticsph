# Here you can find the latest version of the codes to control the robot(s) and the codes for the behavioral experiments

- Bernasconi, Blondiaux et al., 
- DOI: TBC
- fosco.bernasconi@gmail.com 


# Robots
Codes necessary to control the robotic devices

- ./Behavior: code necessary to control the behavioral robot
- ./MR: code necessary to control the MR robot

# Experiments
Codes necessary to control the robotic devices with experimental paradigms (Behavior, MRI, EEG)

- ./Experiments/PH_Experiments.mlapp: MATLAB aplication which allows a user to launch PH experiments. This is the same app for behavioral experiments, EEG      experiments or MRI experiments.
- ./Experiments/EEGAuxFunctions: accessory resources to run EEG experiments (necessary for EEG)
- ./Experiments/MRIntermediate_results: Results from the ongoing experiment are saved in this folder (can be deleted, but will be created automatically every time an experiment is launched)
- ./Experiments/WindowAPI: API by Jan Simon, to appropriately display content in a matlab window
- ./Experiments/tcp_udp_ip: accessory functions to communicate with the program controlling the Robot
- ./Experiments/Templates/: examples of default parameters that can be used for exeriments

# Run the Experiments
To Run the experiment: i) open MATLAB, ii) Browse to ./Experiments, iii)type "PH_Experiments" in the Command Window, this will
open the GUI. Avoid double clicking the PH_Experiment.malpp as this will open the app designer.

# GUI parameters
Here you can find a description of all the parameter that can be changed in the GUI (based on your research questions/needs)


- Participant information: In this tab you can fill in information regarding your participant. Namely, ID, age (must be numeric), handedness and sex at birth. The filename field will define the name of the .mat file to which you experimental results are saved to. If you have different sessions of the same participant make sure to change the name at each session.

- Experiment:    
    - Number of trials: the number of trials of your experiment
    - Trial type: this defines how the duration of your trials are counted. "Seconds" will count a duration in seconds (e.g., 10 seconds), "Pokes back" will define that the duration fo a trial is when X pokes on the back are detected. "Pokes front" will do the same as pokes back, but for pokes on the front robot. Please note that this is forced to "Seconds" if you are in MRI mode
    - Trial duration: the duration of each trial as indicated in trial type. Note that you can input various trial durations. This number must be divisible by the number of trials, so that the design can be counterbalanced.
    - Max duration in sequence: This parameter can be used when inputing multiple trial duration. If you input a trial duration of 10 seconds and another of 20 seconds, you might want that no more than 3 consecutive trials have the same duration. By setting this to "X", the app will guarantee that the randomization of trial durations will never have more than "X" consecutive trials of the same duration. Please note that by setting this to "Inf" (infinite in MATLAB code), you allow for any number of consecutive durations Also note that if you input only one trial duration, you must set this parameter to either Inf, or to the number of trials.
    - Delays: the delay(s) that the robot will use in each trial. Again you might input different delays to be used in different trials, as long as the number of trials is a multiple of the number of delays, as to allow for a balanced design.
    - Max delays in sequence: same logic as "max duration in sequence" but for delays
    - White noise: "off" will not produce white noise in the background, "on" will produce white noise in the background
    - Continue on button press: for behavioral and EEG experiments, you might decide that a trial should only continue if the "Continue experiment" button is pressed. To activate this behaviour set the slider to "On", if the slider is set to "off" a trial will follow another trial after, the value input in Inter-trial interval
    - Inter-trial interval: If in use, defines the amount of time the program waits between trials (in seconds)

- Responses:  
    - If active: In case you activate this behaviour, once a trial will be finished, an answer will be expected from your participant. How that answer is provided depends on if "Display" is active or not. The default behaviour is the one described in answers. If you activate display, please refer to display, and select choice.
    - Answers (multiple answers with enter): this field allows you to input what is accepted as an answer after each trial. For example if you are running an experiment in which participants verbally respond to having experienced a PH or not, you can input in this field "yes no" (separated by enter rather than a space), and then during the actual experiment, inputting "yes" or "no" in the answer input box, will be counted as a valid answer.
    - Display: If active, this option will allow you to show a question on screen, as well as display the options you have written, below the question. The question/presented info should be written in the input area that is below the display checkbox.  Be mindfull that although the program tries to adapt automatically to the amount of options, if you provide several lengthy options for the answers, they might appear out of the screen.
    - Screen Number: The screen that will be blacked out for question and answer presentation. You can check this on your computers display settings.
    - Select choice: Note that this option only works if display is on. If "off" the behaviour of display will be the same as described in answers. Participants will see the question/information on the screen alongside with the options, but the program will still expect an answer to be input in the answer edit box. This can be done by yourself, if the participant verbally reports it, or directly by the participant if they have an answering pad and you make sure the mouse is on the answer edit box. Howver, if you want participants to selected an answer, it is recommended to move the switch to "On". This will allow you to select 3 control keys. One to move left, one to move right, and one to select the option. With this, participants will be able to select one of the options that is below (the option currently selected will be highlighted in a brighter colour).
    - L/R/Select: Note that this only works with display and select choice active. The keys for the selecting an answer in the order left, right, confirm answer. Note that you are expected to present the key code separated by a slash (/). A key code can be checked online on key code tables. Please note that numerical keys have different codes depending if you have the number above the letters, or the keypad numbers. Whereas a keypad number will have a certain code (e.g., 51), the number five above the letters in a keyboard has the code 5% .
    **- Note: Currently display only works in the MRI setting.**

    
- Robot: Default values of transfer and receiver ports should always work for the setting where both MVS and this program are running on the same computer. For an MRI setup you will probably have to check the appropriate ports.
    - Transfer port: port used to send information to the MVS program that controls the robot
    - Receiver port: port used to receive information to the MVS program that controls the robot
    - Remote IP: IP of the computer running the MVS program. Set to localhost if using the same computer for both.
- MRI:
    - Activate MRI settings: This activates the MRI mode. Note that you cannot use any "trial type" other than seconds, with MRI mode. Note as well that if you activate answer settings with MRI, it will force display to be active. Note as well, that the inter-trial interval will be forced to be defined. This will equate to a rest condition between conditions.
    - TR: The repetition time of your experiment
    - Trigger code: The MRI console should be sending a trigger for the beginning of every scan. Here you should input the key code. '5%' corresponds to what's used by most Siemens platforms. '5%' is the key code for the '5' key above the letters in a keyboard (different than the numpad 5)
    - Override delayed triggers: If "on" this will make the program continue anytime a delay of 2.5% is detected when receiving TR triggers. Ideally you will never use this, but if for any reason you suspect your computer is not processing some triggers this option will guarantee that your experiments continues as expected.
    - "Mode": "Experimental", the robot will be moving both parts (front and back). "Touch Localiser" only the back part of the robot will be moving, and should be used as the name indicates for touch localizers. "Movement localiser" only the front part of the robot will be moving, and should be used as the name indicates, for movement localisers.
- EEG:
    - Activate EEG settings: This will activate be EEG settings. Everything runs as a normal behavioral experiment, however, triggers will be sent to the EEG recording system.
    - Equipment: You can choose between serial and parallel port, depending on your setup.
    - Serial port: the program should detect automatically the available serial ports. You must select the desired one. Enabled only in serial equipment
    - Parallel port: input the address of your parallel port, if in parallel port mode. This can be checked on your computer's device manager menu.

# Important notes 
When using PH_Experiments.mlapp, make sure you have installed MATLAB 2019b, or any more recent version.
You are also required to have some toolboxes installed. This is informed in case they are not detected.
In principle the Receiver and transfer ports for robot communication should be as default for both behavior and MRI experiments, however, your specific needs might require you to investigate the appropriate port codes, and change them in the GUI (Robot tab).

